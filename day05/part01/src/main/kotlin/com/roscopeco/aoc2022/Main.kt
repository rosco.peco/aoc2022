package com.roscopeco.aoc2022

class Main

// This is _horrible_ but it works :D
fun main(args: Array<String>) {
    val input = Main::class.java.getResource("/input.txt")?.readText()!!

    val (stackIn, moveIn) = input.split("\n\n").map { it.split("\n") }

    val stacksRaw = stackIn
        .filter { it.contains("[") }                        // filter out stack numbers
        .map {
            it.replace(Regex("[]\\[]"), " ")                // Replace brackets with spaces
                .replace(Regex("^\\s"), "")                 // Remove single space at start of line
                .replace(Regex("\\s{3}"), " ")              // Replace runs of three spaces with one
        }.reversed()

    stacksRaw.forEach { println(it) }

    val stacks = runMoves(moveIn, populateStacks(stacksRaw))

    println("\n\nResult: ${stacks.map { it!!.first() }.joinToString("")}")
}

fun populateStacks(stacks: List<String>): List<ArrayDeque<Char>?> {
    val result = mutableListOf<ArrayDeque<Char>?>()

    stacks.forEach { line -> run { line.forEachIndexed { i, c ->
        if (c in 'A'..'Z') {
            val stack = result.getOrNull(i / 2) ?: run {
                val new = ArrayDeque<Char>()
                result.add(i / 2, new)
                new
            }

            stack.addFirst(c)

        }
    } } }

    return result
}

fun runMoves(moveIn: List<String>, stacks: List<ArrayDeque<Char>?>): List<ArrayDeque<Char>?> {
    moveIn.map {
        it.replace(Regex("[^\\s\\d]"), "")                  // Remove non-whitespace, non-digits
            .replace(Regex("^\\s"), "")                     // Remove single space at start of line
            .split(Regex("\\s+"))                           // Split on (any) whitespace
            .map { n -> n.toInt() }
    }.forEach { (num, from, to) ->
        for (i in 0 until num) {
            stacks[to - 1]!!.addFirst(stacks[from - 1]!!.removeFirst())
        }
    }

    return stacks
}