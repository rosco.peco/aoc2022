package com.roscopeco.aoc2022

class Main

fun main(args: Array<String>) {
    val input = Main::class.java.getResource("/input.txt")?.readText()!!

    val result = input.lines()
        .map { line -> listOf(line.substring(0, line.length / 2), line.substring(line.length / 2, line.length)) }
        .map { sack -> sack.map { comp -> comp.split(Regex("")).toMutableSet().minus("") } }
        .map { sack -> sack[0].intersect(sack[1]) }
        .map { inters -> inters.map { charsValue(it) } }
        .map { inter -> inter.sum() }
        .sum()

    println(result)
}

fun charsValue(string: String): Int {
    return string.map { c ->
        when (c.code) {
            in 65..90 -> c.code - 38
            in 97..122 -> c.code - 96
            else -> throw IllegalArgumentException("Unexpected char: $c")
        }
    }.sum()
}