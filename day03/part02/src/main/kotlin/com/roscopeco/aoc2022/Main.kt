package com.roscopeco.aoc2022

class Main

fun main(args: Array<String>) {
    val input = Main::class.java.getResource("/input.txt")?.readText()!!

    val result = input.lines()
        .asSequence()
        .chunked(3)
        .map { group -> group.map { comp -> comp.split(Regex("")).toMutableSet() } }
        .map { group -> group.reduce { acc, it -> acc.apply { retainAll(it) } }.minus("") }
        .map { inters -> inters.map { charsValue(it) } }
        .sumOf { inter -> inter.sum() }

    println(result)
}

fun charsValue(string: String): Int {
    return string.map { c ->
        when (c.code) {
            in 65..90 -> c.code - 38
            in 97..122 -> c.code - 96
            else -> throw IllegalArgumentException("Unexpected char: $c")
        }
    }.sum()
}