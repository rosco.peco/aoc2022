package com.roscopeco.aoc2022.day01

class Main

fun main(args: Array<String>) {
    val input = Main::class.java.getResource("/input.txt")?.readText()!!

    val results = input.split("\n\n")
        .map { it.lines().map { s -> s.toInt() }.reduce { a, b -> a + b } }
        .sortedDescending()

    println(results.subList(0, 3).sum())
}
