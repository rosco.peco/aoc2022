package com.roscopeco.aoc2022.day01

class Main

fun main(args: Array<String>) {
    val input = Main::class.java.getResource("/input.txt")?.readText()!!

    println(input.split("\n\n")
        .map { it.lines().map { s -> s.toInt() }.reduce { a, b -> a + b } }.maxOf { it }
    )
}
