package com.roscopeco.aoc2022

import kotlin.math.truncate

class Main

fun main(args: Array<String>) {
    val monkeys = Main::class.java.getResource("/input.txt")?.readText()!!
        .split("\n\n")
        .map { Monkey(it) }

    val rounds = 20

    (0 until rounds).forEach { roundNum ->
        println("Round $roundNum")
        println("========")

        monkeys.forEach { monkey ->
            println("Monkey ${monkey.num}:")
            monkey.items.forEach { item ->
                println("  Monkey inspects an item with a worry level of $item")
                var newItem = applyArithmeticOp(item, monkey.op)
                println("    Operator ${monkey.op} is applied, worry level now $newItem")
                val divided = truncate(newItem.toFloat() / 3).toInt()
                println("    Monkey gets bored with item. Worry level is divided by 3 to $divided")

                val targetMonkey = if (divided % monkey.testDivisor == 0) {
                    println("    Current worry level is divisible by ${monkey.testDivisor}")
                    monkey.trueMonkey
                } else {
                    println("    Current worry level is not divisible by ${monkey.testDivisor}")
                    monkey.falseMonkey
                }

                println("    Item with worry level $divided is thrown to monkey $targetMonkey")
                monkeys[targetMonkey].items.add(divided)

                monkey.itemsInspected += 1
            }
            monkey.items.clear()
        }
    }

    println()
    println("After $rounds rounds")
    println("===============")
    monkeys.forEach { monkey ->
        println("Monkey ${monkey.num} inspected items ${monkey.itemsInspected} times")
    }

    val monkeyBusiness = monkeys.sortedBy { it.itemsInspected }
        .reversed()
        .take(2)
        .map { monkey -> monkey.itemsInspected }
        .reduce { a, b -> a * b}

    println()
    println("Level of monkey business is $monkeyBusiness")
}

fun applyArithmeticOp(lhs: Int, op: Pair<ArithmeticOperation, String>): Int {
    val rhs = if (op.second == "old") {
        lhs
    } else {
        op.second.toInt()
    }

    return op.first.apply(lhs, rhs)
}

class Monkey(input: String) {
    val num: Int
    val items: MutableList<Int>
    val op: Pair<ArithmeticOperation, String>
    val testDivisor: Int
    val falseMonkey: Int
    val trueMonkey: Int
    var itemsInspected: Int = 0

    init {
        num = Regex("Monkey (\\d+):").find(input)!!.groupValues[1].toInt()

        items = Regex("Starting items: ([\\d\\s,]+)").find(input)!!.groupValues[1]
            .split(",")
            .map { it.strip().toInt() }
            .toMutableList()

        val opIn = Regex("Operation: new = old ([+\\-*/]) (old|\\d+)").find(input)!!.groupValues

        op = Pair(ArithmeticOperation.fromStr(opIn[1]), opIn[2])

        testDivisor = Regex("Test: divisible by (\\d+)").find(input)!!.groupValues[1].toInt()

        falseMonkey = Regex("If false: throw to monkey (\\d)+").find(input)!!.groupValues[1].toInt()
        trueMonkey = Regex("If true: throw to monkey (\\d)+").find(input)!!.groupValues[1].toInt()
    }

    override fun toString(): String {
        return "Monkey $num - $items : new = old $op : divisor = $testDivisor [false: $falseMonkey, true: $trueMonkey]"
    }
}

enum class ArithmeticOperation(val op: (Int, Int) -> Int) {
    PLUS({a, b -> a + b}),
    MINUS({a, b -> a - b}),
    MULT({a, b -> a * b}),
    DIV({a, b -> a / b});

    fun apply(left: Int, right: Int) = op(left, right)

    companion object {
        fun fromStr(s: String) = when (s) {
            "+" -> PLUS
            "-" -> MINUS
            "*" -> MULT
            "/" -> DIV
            else -> throw IllegalArgumentException("Bad operator $s")
        }
    }
}