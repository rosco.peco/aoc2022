package com.roscopeco.aoc2022

class Main

fun main() {
    val input = Main::class.java.getResource("/input.txt")?.readText()!!.lines()
        .map { Regex("(\\w+)(?:\\s([\\d-]+))?").matchEntire(it)!! }
        .map { Pair(it.groupValues[1], if (it.groupValues[2].isEmpty()) 0 else it.groupValues[2].toInt()) }

    val machine = Machine(input)

    while (machine.tick()) {
        // continue
    }
}

class Machine(private val program: List<Pair<String, Int>>) {
    private var insnCyclesRemain = 0
    private var pc = 0
    var totalTicks = 0
    var x = 1
    var screen = Screen()

    init {
        insnCyclesRemain = if (program[0].first == "addx") 1 else 0
    }

    fun tick(): Boolean {
        totalTicks += 1

        screen.tick(x)

        if (insnCyclesRemain == 0) {
            if (program[pc].first == "addx") {
                x += program[pc].second
            }

            pc += 1

            return if (pc == program.size) {
                false
            } else {
                insnCyclesRemain = if (program[pc].first == "addx") 1 else 0
                true
            }
        } else {
            insnCyclesRemain -= 1
            return true
        }
    }
}

class Screen {
    private var totalTicks = 0
    private var xpos = 0

    fun tick(x: Int) {
        if (((xpos-1) ..(xpos+1)).contains(x)) {
            // visible pixel
            print("#")
        } else {
            print(" ")
        }

        xpos += 1
        totalTicks += 1

        if (totalTicks > 0 && totalTicks % 40 == 0) {
            xpos = 0
            println()
        }
    }
}