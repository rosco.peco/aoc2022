package com.roscopeco.aoc2022

class Main

fun main() {
    val input = Main::class.java.getResource("/input.txt")?.readText()!!.lines()
        .map { Regex("(\\w+)(?:\\s([\\d-]+))?").matchEntire(it)!! }
        .map { Pair(it.groupValues[1], if (it.groupValues[2].isEmpty()) 0 else it.groupValues[2].toInt()) }

    val machine = Machine(input)
    val strengths = mutableListOf<Int>()

    while (machine.tick()) {
        if (machine.totalTicks == 19 || machine.totalTicks == 59 || machine.totalTicks == 99 || machine.totalTicks == 139 || machine.totalTicks == 179 || machine.totalTicks == 219) {
            strengths += (machine.x * (machine.totalTicks + 1))
        }
    }

    println("${strengths.sum()} : $strengths")
}

class Machine(private val program: List<Pair<String, Int>>) {
    private var insnCyclesRemain = 0
    private var pc = 0
    var totalTicks = 0
    var x = 1

    init {
        insnCyclesRemain = if (program[0].first == "addx") 1 else 0
    }

    fun tick(): Boolean {
        println("$totalTicks: ${program[pc]} [px: $pc, x: $x]")
        totalTicks += 1
        if (insnCyclesRemain == 0) {
            if (program[pc].first == "addx") {
                x += program[pc].second
            }

            pc += 1

            return if (pc == program.size) {
                false
            } else {
                insnCyclesRemain = if (program[pc].first == "addx") 1 else 0
                true
            }
        } else {
            insnCyclesRemain -= 1
            return true
        }
    }
}