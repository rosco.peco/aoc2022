package com.roscopeco.aoc2022.day02

class Main

enum class Move(val score: Int) {
    ROCK(1),
    PAPER(2),
    SCISSORS(3);

    companion object {
        fun fromString(str: String): Move = when (str) {
            "A" -> ROCK
            "B" -> PAPER
            "C" -> SCISSORS
            else -> throw java.lang.IllegalArgumentException("Bad move: $str")
        }

        fun calcScore(them: Move, me: Move) = when {
            them == me -> me.score + 3
            them.beats == me -> me.score
            me.beats == them -> me.score + 6
            else -> throw IllegalArgumentException("Error calculating score for $them vs $me")
        }
    }

    val beats: Move
        get() = when (this) {
            ROCK -> SCISSORS
            PAPER -> ROCK
            SCISSORS -> PAPER
        }

   val losingMove: Move
        get() = when(this) {
            ROCK -> SCISSORS
            PAPER -> ROCK
            SCISSORS -> PAPER
        }

    val drawingMove: Move
        get() = this

    val winningMove: Move
        get() = when(this) {
            ROCK -> PAPER
            PAPER -> SCISSORS
            SCISSORS -> ROCK
        }
}

fun getMyMove(them: Move, desired: String) = when(desired) {
    "X" -> them.losingMove
    "Y" -> them.drawingMove
    "Z" -> them.winningMove
    else -> throw IllegalArgumentException("Unknown desired outcome $desired")
}


fun main(args: Array<String>) {
    val input = Main::class.java.getResource("/input.txt")?.readText()!!
    val score = input.lines()
        .map { it.split(" ") }
        .map { (them, desired) -> listOf(Move.fromString(them), getMyMove(Move.fromString(them), desired)) }
        .sumOf { (them, me) -> Move.calcScore(them, me) }

    println(score)
}