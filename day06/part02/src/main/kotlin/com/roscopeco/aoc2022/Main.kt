package com.roscopeco.aoc2022

class Main

fun main(args: Array<String>) {
    val input = Main::class.java.getResource("/input.txt")?.readText()!!
    val chars = input.toCharArray().toList()
    val windowSize = 14
    var windowStart = 0

    while (windowStart < chars.size - 3) {
        val window = chars.subList(windowStart, windowStart + windowSize)
        if (window.distinct().size == windowSize) {
            println("SOP found at ${windowStart + windowSize}")
            break
        }
        windowStart += 1
    }
}