package com.roscopeco.aoc2022

class Main

fun main(args: Array<String>) {
    val input = Main::class.java.getResource("/input.txt")?.readText()!!
    val chars = input.toCharArray().toList()
    var windowStart = 0

    while (windowStart < chars.size - 3) {
        val window = chars.subList(windowStart, windowStart + 4)
        if (window.distinct().size == 4) {
            println("SOP found at ${windowStart + 4}")
            break
        }
        windowStart += 1
    }
}