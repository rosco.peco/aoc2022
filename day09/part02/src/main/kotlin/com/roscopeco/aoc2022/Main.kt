package com.roscopeco.aoc2022

class Main

fun main() {
    val input = Main::class.java.getResource("/input.txt")?.readText()!!.lines()
        .map { line -> Regex("([LRUD]) (\\d+)").matchEntire(line)!!.groupValues.toTypedArray() }
        .map { matches -> Pair(matches[1], matches[2].toInt()) }

    val visited = mutableSetOf(Position(0, 0))
    val sim = Sim(visited, 10)

    input.forEach { move ->
        sim.direction = move.first
        sim.steps = move.second
        sim.run()
    }

    println(visited.size)
}

data class Position(val x: Int, val y: Int) {
    fun touchingPositions(): List<Position> = listOf(
        Position(x - 1, y - 1),
        Position(x, y - 1),
        Position(x + 1, y - 1),
        Position(x - 1, y),
        Position(x, y),
        Position(x + 1, y),
        Position(x - 1, y + 1),
        Position(x, y + 1),
        Position(x + 1, y + 1)
    )
}

interface Segment {
    fun getPosition(): Position
    fun setPosition(newPos: Position)
    fun getHeadPosition(): Position
}

class TailSegment(private val headSegment: Segment, private var pos: Position) : Segment {
    override fun getPosition(): Position = pos

    override fun setPosition(newPos: Position) {
        pos = newPos
    }

    override fun getHeadPosition() = headSegment.getPosition()
}

class HeadSegment(var pos: Position) : Segment {
    override fun getPosition(): Position = pos

    override fun setPosition(newPos: Position) {
        pos = newPos
    }

    override fun getHeadPosition() = pos
}

class Sim(private val positions: MutableSet<Position>, snakeSize: Int) {
    private var head = HeadSegment(Position(0, 0))
    private var tail = buildTail(head, snakeSize - 1)
    var direction = "U"
    var steps = 0

    fun run() {
        println("Run: steps = $steps")
        while (steps > 0) {
            step()
        }
    }

    private fun buildTail(head: Segment, size: Int): List<Segment> {
        var last = head
        return (0 until size).map { last = TailSegment(last, Position(0, 0)); last }
    }

    private fun step() {
        println("    Step: Dir = ${direction}, steps = $steps")
        if (steps > 0) {
            when (direction) {
                "U" -> head.pos = Position(head.pos.x, head.pos.y + 1)
                "D" -> head.pos = Position(head.pos.x, head.pos.y - 1)
                "L" -> head.pos = Position(head.pos.x - 1, head.pos.y)
                "R" -> head.pos = Position(head.pos.x + 1, head.pos.y)
                else -> throw IllegalArgumentException("Invalid direction: $direction")
            }

            tail.forEach { updateTail(it) }

            println("        head: ${head}, tail: $tail")

            positions.add(tail.last().getPosition())

            steps -= 1
        }
    }

    private fun updateTail(seg: Segment) {
        val head = seg.getHeadPosition()
        val tail = seg.getPosition()
        val touchingPositions = head.touchingPositions()

        if (!touchingPositions.contains(seg.getPosition())) {
            if (head.x < tail.x) {
                // is to left
                seg.setPosition(if (head.y < tail.y) {
                    // is down and left
                    Position(tail.x - 1, tail.y - 1)
                } else if (head.y > tail.y) {
                    // is up and left
                    Position(tail.x - 1, tail.y + 1)
                } else {
                    // is just left
                    Position(tail.x - 1, tail.y)
                })
            } else if (head.x > tail.x) {
                // is to right
                seg.setPosition(if (head.y < tail.y) {
                    // is down and right
                    Position(tail.x + 1, tail.y - 1)
                } else if (head.y > tail.y) {
                    // is up and right
                    Position(tail.x + 1, tail.y + 1)
                } else {
                    // is just right
                    Position(tail.x + 1, tail.y)
                })
            } else {
                seg.setPosition(if (head.y < tail.y) {
                    // is down
                    Position(tail.x, tail.y - 1)
                } else {
                    // is up
                    Position(tail.x, tail.y + 1)
                })
            }
        }
    }
}
