package com.roscopeco.aoc2022

class Main

data class Position(val x: Int, val y: Int) {
    fun touchingPositions(): List<Position> = listOf(
        Position(x - 1, y - 1),
        Position(x, y - 1),
        Position(x + 1, y - 1),
        Position(x - 1, y),
        Position(x, y),
        Position(x + 1, y),
        Position(x - 1, y + 1),
        Position(x, y + 1),
        Position(x + 1, y + 1)
    )
}

class Sim(val positions: MutableSet<Position>) {
    var headPos = Position(0, 0)
    var tailPos = Position(0, 0)
    var direction = "U"
    var steps = 0

    fun run() {
        println("Run: steps = $steps")
        while (steps > 0) {
            step()
        }
    }

    fun step() {
        println("    Step: Dir = ${direction}, steps = $steps")
        if (steps > 0) {
            when (direction) {
                "U" -> headPos = Position(headPos.x, headPos.y + 1)
                "D" -> headPos = Position(headPos.x, headPos.y - 1)
                "L" -> headPos = Position(headPos.x - 1, headPos.y)
                "R" -> headPos = Position(headPos.x + 1, headPos.y)
                else -> throw IllegalArgumentException("Invalid direction: $direction")
            }

            updateTail()

            println("        headPos: ${headPos}, tailPos: $tailPos")

            positions.add(tailPos)

            steps -= 1
        }
    }

    private fun updateTail() {
        val touchingPositions = headPos.touchingPositions()

        if (!touchingPositions.contains(tailPos)) {
            if (headPos.x < tailPos.x) {
                // is to left
                tailPos = if (headPos.y < tailPos.y) {
                    // is down and left
                    Position(tailPos.x - 1, tailPos.y - 1)
                } else if (headPos.y > tailPos.y) {
                    // is up and left
                    Position(tailPos.x - 1, tailPos.y + 1)
                } else {
                    // is just left
                    Position(tailPos.x - 1, tailPos.y)
                }
            } else if (headPos.x > tailPos.x) {
                // is to right
                tailPos = if (headPos.y < tailPos.y) {
                    // is down and right
                    Position(tailPos.x + 1, tailPos.y - 1)
                } else if (headPos.y > tailPos.y) {
                    // is up and right
                    Position(tailPos.x + 1, tailPos.y + 1)
                } else {
                    // is just right
                    Position(tailPos.x + 1, tailPos.y)
                }
            } else {
                tailPos = if (headPos.y < tailPos.y) {
                    // is down
                    Position(tailPos.x, tailPos.y - 1)
                } else {
                    // is up
                    Position(tailPos.x, tailPos.y + 1)
                }
            }
        }
    }
}

fun main() {
    val input = Main::class.java.getResource("/input.txt")?.readText()!!.lines()
        .map { line -> Regex("([LRUD]) (\\d+)").matchEntire(line)!!.groupValues.toTypedArray() }
        .map { matches -> Pair(matches[1], matches[2].toInt()) }

    val visited = mutableSetOf(Position(0, 0))
    val sim = Sim(visited)

    input.forEach { move ->
        sim.direction = move.first
        sim.steps = move.second
        sim.run()
    }

    println(visited.size)
}


