package com.roscopeco.aoc2022

class Main

fun main() {
    val input = Main::class.java.getResource("/input.txt")?.readText()!!.lines()
    val grid = input.map { line -> line.toCharArray().map { c -> c.digitToInt() }.toList() }

    var best = 0
    (1 until grid.first().size - 1).forEach { y -> (1 until grid.size - 1).forEach { x ->
        val score = treesVisible(x, y, grid).score()
        println("$y,$x (${grid[y][x]}) score is $score")
        if (score > best) best = score
    } }

    println(best)
}

data class Visibility(val up: Int, val down: Int, val left: Int, val right: Int) {
    fun score() = up * down * left * right
}

fun treesVisible(x: Int, y: Int, grid: List<List<Int>>): Visibility {
    val tree = grid[y][x]
    var visibleL = 0
    var visibleR = 0
    var visibleU = 0
    var visibleD = 0

    // from left?
    for (nx in ((x - 1).downTo(0))) {
        visibleL += 1
        if (grid[y][nx] >= tree) break
    }

    // from right?
    for (nx in (x + 1) until grid.first().size) {
        visibleR += 1
        if (grid[y][nx] >= tree) break
    }

    // from up?
    for (ny in (y - 1).downTo(0)) {
        visibleU += 1
        if (grid[ny][x] >= tree) break
    }

    // from down?
    for (ny in ((y + 1) until grid.size)) {
        visibleD += 1
        if (grid[ny][x] >= tree) break
    }

    return Visibility(
        visibleU,
        visibleD,
        visibleL,
        visibleR
    )
}
