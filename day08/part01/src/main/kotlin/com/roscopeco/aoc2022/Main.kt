package com.roscopeco.aoc2022

class Main

fun main(args: Array<String>) {
    val input = Main::class.java.getResource("/input.txt")?.readText()!!.lines()
    val grid = input.map { line -> line.toCharArray().map { c -> c.digitToInt() }.toList() }
    var visibleCount = perimeterTrees(grid)

    (1 until grid.first().size - 1).forEach { y -> (1 until grid.size - 1).forEach { x ->
        if (treeVisible(x, y, grid).any()) {
            visibleCount += 1
        }
    } }

    println(visibleCount)
}

fun perimeterTrees(grid: List<List<Int>>) = 2 * (grid.size + grid.first().size) - 4

data class Visibility(val up: Boolean, val down: Boolean, val left: Boolean, val right: Boolean) {
    fun any() = up || down || left || right
}

fun treeVisible(x: Int, y: Int, grid: List<List<Int>>): Visibility {
    val tree = grid[y][x]
    var highestL = 0
    var highestR = 0
    var highestU = 0
    var highestD = 0

    // from left?
    ((x - 1).downTo(0)).forEach { nx -> if (grid[y][nx] > highestL) highestL = grid[y][nx] }
    // from right?
    ((x + 1) until grid.first().size).forEach { nx -> if (grid[y][nx] > highestR) highestR = grid[y][nx] }
    // from up?
    ((y - 1).downTo(0)).forEach { ny -> if (grid[ny][x] > highestU) highestU = grid[ny][x] }
    // from down?
    ((y + 1) until grid.size).forEach { ny -> if (grid[ny][x] > highestD) highestD = grid[ny][x] }

    println("[$x,$y]: $tree -> Highest[UDLR] = [${highestU},${highestD},${highestL},${highestR}]: Visible [UDLR] = [${highestU < tree},${highestD < tree},${highestL < tree},${highestR < tree}]")
    return Visibility(
        highestU < tree,
        highestD < tree,
        highestL < tree,
        highestR < tree
    )
}
