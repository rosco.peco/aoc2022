## Advent of Code 2022

I'm starting out in Kotlin this time.

Maybe I'll switch to something else later, maybe not 🤷

As usual, the first commit for each day is the unedited first iteration of the code that I used to just get the answers. This will often be a quickly hacked-together solution intended to get to the answer as quickly as possible. On some days I might then tidy things up and refactor a bit in subsequent commits.
