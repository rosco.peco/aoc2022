package com.roscopeco.aoc2022

class Main

fun main(args: Array<String>) {
    val input = Main::class.java.getResource("/input.txt")?.readText()!!

    val result = input.lines()
        .map { it.split(",") }
        .map { line -> line.map { range -> range.split("-") } }
        .map { line -> line.map { range -> IntRange(range.first().toInt(), range.last().toInt()) } }
        .map { line -> line.map { range -> range.toSet() } }
        .map { line -> line.first().intersect(line.last()).isNotEmpty() }
        .filter { it }
        .size

    println(result)

}