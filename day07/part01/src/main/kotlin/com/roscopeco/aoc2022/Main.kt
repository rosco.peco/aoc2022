package com.roscopeco.aoc2022

class Main

fun main() {
    val input = Main::class.java.getResource("/input.txt")?.readText()!!.lines()
    println(Filesystem(input).findDirsMatching { it.size() < 100000 }.sumOf { it.size() })
}

data class Directory(val parent: Directory?, val name: String, val subDirs: MutableList<Directory>, val files: MutableList<Pair<String, Int>>) {
    constructor(name: String) : this(null, name, mutableListOf(), mutableListOf())
    constructor(parent: Directory?, name: String) : this(parent, name, mutableListOf(), mutableListOf())

    fun size(): Int = subDirs.sumOf { it.size() } + files.sumOf { it.second }

    fun findSubdirsMatching(predicate: (Directory) -> Boolean): List<Directory> {
        val out = mutableListOf<Directory>()
        subDirs.forEach { findMatchingDirs(it, out, predicate) }
        return out
    }

    private fun findMatchingDirs(dir: Directory, out: MutableList<Directory>, predicate: (Directory) -> Boolean): List<Directory> {
        if (predicate.invoke(dir)) {
            out.add(dir)
        }

        dir.subDirs.forEach { findMatchingDirs(it, out, predicate) }
        return out
    }

    override fun toString(): String {
        return "DIR <$name> : $files : size = ${size()} with ${subDirs.size} subdirs"
    }
}

class Filesystem(val root: Directory) {
    constructor(commandList: List<String>) : this(runCommands(commandList))

    fun findDirsMatching(predicate: (Directory) -> Boolean) = root.findSubdirsMatching(predicate)

    companion object {
        private fun runCommands(commandList: List<String>): Directory {
            val stack = mutableListOf(Directory("/"))

            commandList.forEach { parseLine(stack, it) }

            return stack.first()
        }

        private fun parseLine(stack: MutableList<Directory>, cmd: String) {
            when {
                Regex("\\$ cd /").matches(cmd) -> cdRoot(stack)
                Regex("\\$ cd ..").matches(cmd) -> cdUp(stack)
                Regex("\\$ cd [A-Za-z.]+").matches(cmd) -> cd(stack, cmd)
                Regex("\\d+ [A-Za-z.]+").matches(cmd) -> addFile(stack, cmd)
            }
        }

        private fun cdRoot(stack: MutableList<Directory>) {
            stack.removeAll { it.name != "/" }
        }

        private fun cdUp(stack: MutableList<Directory>) {
            stack.removeLast()
        }

        private fun cd(stack: MutableList<Directory>, line: String) {
            val current = stack.last()
            val new = Directory(current, Regex("\\$ cd ([A-Za-z.]+)").matchEntire(line)!!.groupValues[1])
            current.subDirs.add(new)
            stack.add(new)
        }

        private fun addFile(stack: MutableList<Directory>, line: String) {
            val (name, size) = Regex("(\\d+) ([A-Za-z.]+)").matchEntire(line)!!.groupValues
            stack.last().files.add(Pair(name, size.toInt()))
        }
    }
}
